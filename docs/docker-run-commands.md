# Ansible10 Image

```
docker build . -t ansible10-local -f <DOCKERFILE_PATH>

docker run -it --name ansible10-local -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock ansible10-local /bin/bash

docker run -it --name ansible10 -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock ansible10-local /bin/bash

docker run -it --name terrible2 -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock docker.rd1.thingworx.io/infrastructure/terrible/devops/terrible:2.0.0 /bin/bash
```

<!-- use terrible from gitlab -->

```
docker run -it --name terrible -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock docker.rd1.thingworx.io/infrastructure/terrible/devops/terrible:2.1.1 /bin/bash
```

# Terrible image with AWS CLI

``` bash
# Run container, mount .ssh, docker socket and gitlab directory
$ docker run -it --name terrible -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock docker.rd1.thingworx.io/infrastructure/terrible/devops/terrible:2.1.1 /bin/bash

# Run container, mount .ssh, docker socket and gitlab directory. Mount aws script for running commands
$ docker run -it --name gitlab-terrible -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab docker.rd1.thingworx.io/ligre-squad/ansible-playbooks/gitlab:2.2.1 /bin/bash

# 
docker run -it --rm -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /c/Users/chanlon/.aws:/root/.aws terrible:latest /bin/bash

# run setup script to run ssh-agent and load AWS file to be local bin
$ . /gitlab/personal/local-dev/docker/docker-setup.sh
```

**FOR MOUNTING WITH RANCHER DESKTOP**
Mounting is done via WSL, where `/mnt/c/` is the base as opposed to just `/c/`

<!-- use docker from Harbor -->

```
docker run -it --name terrible -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock studio-docker.rd1.thingworx.io/terrible/devops/terrible-ansible10:2.1.1 /bin/bash
```

# Deploy-Hub GitLab Dir image

```
docker run -it --name dh -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /c/Users/chanlon/.ssh:/.ssh -v /c/Users/chanlon/gitlab:/gitlab -e BUNDLE_GEMFILE=/gitlab/docker/deploy-hub/Gemfile -e GITLAB_API_PRIVATE_TOKEN ruby /bin/bash
```

```
bundle exec ruby -I /gitlab/docker/deploy-hub/lib /gitlab/docker/deploy-hub/bin/release
bundle exec ruby -I /gitlab/docker/deploy-hub/lib /gitlab/docker/deploy-hub/bin/apply-version-tag
```

# studio-aws playbooks

```
docker run -it --rm -v /c/Users/chanlon/gitlab/ligre-squad/studio-aws:/src -v /c/Users/chanlon/.aws:/root/.aws studio-docker.rd1.thingworx.io/terrible/devops/terrible:latest sh -c "cd /src && pip install --upgrade boto3 --break-system-packages && pip install --upgrade ansible --break-system-packages && ansible-galaxy collection install amazon.aws -vvv && export AWS_PROFILE=Vuforia-Policy-SSO-585826603973 && ansible-playbook aws.yml --check"
```

# Shellcheck

```
docker run -it --name shellcheck -v /c/Users/chanlon/gitlab:/gitlab koalaman/shellcheck-alpine:stable /bin/bash
```

```
shellcheck --version
find . -name "*.sh" | xargs -n 1 shellcheck ${SHELLCHECK_OPTS}
```

# Upgrade

```pwsh
docker run -it --name terrible11-14 -e VAULT_USERNAME -e VAULT_PASSWORD -e VAULT_ADDR -v /mnt/c/Users/chanlon/.ssh:/.ssh -v /mnt/c/Users/chanlon/gitlab:/gitlab -v /var/run/docker.sock:/var/run/docker.sock docker.rd1.thingworx.io/infrastructure/terrible/devops/terrible:2.1.1 /bin/bash
```

# Jenkins Image
https://www.jenkins.io/doc/book/installing/docker/#on-windows

```PowerShell
$JENKINS_VERSION="2.426.3"

docker network create jenkins

docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind

# docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker `
#   --env DOCKER_TLS_CERTDIR=/certs `
#   --volume jenkins-docker-certs:/certs/client `
#   --volume jenkins-data:/var/jenkins_home `
#   --publish 2376:2376 `
#   docker:dind

docker build -t jenkins-local:$JENKINS_VERSION . -f Dockerfile.jenkins --build-arg="JENKINS_VERSION=$JENKINS_VERSION"

docker run --name jenkins-$JENKINS_VERSION --restart=on-failure --detach --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 --volume jenkins-data:/var/jenkins_home --volume jenkins-docker-certs:/certs/client:ro --publish 8080:8080 --publish 50000:50000 jenkins-local:$JENKINS_VERSION

docker exec -it jenkins-$JENKINS_VERSION bash
```