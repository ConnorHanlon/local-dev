# install Git

https://git-scm.com/download/win

# download VSCode

https://code.visualstudio.com/download

# download Obsidian

https://obsidian.md/download

# download Docker Desktop

https://www.docker.com/products/docker-desktop/

## update wsl

```powershell
wsl update
```

# download Slack

https://slack.com/downloads/windows

# install NVM

https://github.com/coreybutler/nvm-windows
https://github.com/coreybutler/nvm-windows/releases

# install Chocolatey

https://chocolatey.org/install

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

# install vault

https://developer.hashicorp.com/vault/tutorials/getting-started/getting-started-install

# install onepass extension

Add 1Password chrome extension

# clone local-dev, setup bashrc and gitconfig

1. Login to gitlab
2. Clone local-dev repo via HTTPS
3. Follow README steps for setting up bashrc and gitconfig

# download Postman

https://www.postman.com/downloads/

# Install make

```
choco install make
```

# Install Azure CLI

``` powershell
winget install --exact --id Microsoft.AzureCLI
```

# Install AWS CLI

``` powershell

```