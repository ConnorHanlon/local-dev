# List computer info

## Linux

```
$ lscpu
# RedHat Output
Register this system with Red Hat Insights: insights-client --register
Create an account or view all your systems at https://red.ht/insights-dashboard
[ec2-user@ip-10-76-68-135 ~]$ lscpu
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
CPU(s):              2
On-line CPU(s) list: 0,1
Thread(s) per core:  1
Core(s) per socket:  2
Socket(s):           1
NUMA node(s):        1
Vendor ID:           GenuineIntel
CPU family:          6
Model:               79
Model name:          Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz
Stepping:            1
CPU MHz:             2299.969
BogoMIPS:            4599.99
Hypervisor vendor:   Xen
Virtualization type: full
L1d cache:           32K
L1i cache:           32K
L2 cache:            256K
L3 cache:            46080K
NUMA node0 CPU(s):   0,1
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp lm constant_tsc rep_ovbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm cpuid_fault invpcid_single pti fsgsbase bmi1 avx2 smep bmi2 erms invpcid xsaveop
```

# HashiCorp Vault

```
# change password
vault write auth/userpass/users/{user-name}/password password=NEWPASSWORD
```

## Windows

``` powershell
# Enable long paths for Windows
New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem" -Name "LongPathsEnabled" -Value 1 -PropertyType DWORD -Force

# Enable long paths for Git
git config --system core.longpaths true

# Enable case-sensitive filesystem for specific repo or globally for git (treats example.txt and Example.txt as two different files)
git config core.ignorecase true
git config --global core.ignorecase true
```

# aws

## AWS SSO

``` powershell
# configure AWS profile and sso session.
# https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-sso.html
$ aws configure sso
SSO session name (Recommended): chanlon-sso
SSO start URL [None]: https://vuforia.awsapps.com/start/#
SSO region [None]: us-east-1
SSO registration scopes [None]: sso:account:access

CLI default client Region [None]: us-west-2
CLI default output format [None]: json
CLI profile name [123456789011_ReadOnly]: Vuforia-Policy-SSO-585826603973

## the configuration file will be generated with the sso configuration
## ~/.aws/config
# [sso-session chanlon-sso]
# sso_start_url = https://vuforia.awsapps.com/start/#
# sso_region = us-west-2
# sso_registration_scopes = sso:account:access

# [profile Vuforia-Policy-SSO-585826603973]
# sso_session = chanlon-sso
# sso_account_id = 585826603973
# sso_role_name = Vuforia-Policy-SSO
# region = us-east-1

# login to profile Vuforia-Policy-SSO-585826603973 which uses chanlon-sso. 
# Follow the login prompts.
$ aws sso login --profile Vuforia-Policy-SSO-585826603973
```