FROM alpine:latest

RUN apk update && apk upgrade && \
  apk add build-base git bash make
