#!/bin/sh

# This file should not be required anymore. We login to vault and aws, then run the container and mount the
# auth credentials to the container. The method works with SSM, so SSH keys are not necessary with that approach.
# If need be, ssh keys can be mounted to the container. But, this file is not necessary.

echo "Logging into hvault"
vault login -no-print -method=userpass username="${VAULT_USERNAME}" password="${VAULT_PASSWORD}"

chmod 0400 /.ssh/ligre-squad.pem
chmod 0400 /.ssh/chanlon-ssh.pem
chmod 0400 /.ssh/id_rsa
eval `ssh-agent -s`
ssh-add /.ssh/ligre-squad.pem
ssh-add /.ssh/chanlon-ssh.pem
ssh-add /.ssh/id_rsa

# export AWS_DEFAULT_PROFILE="Vuforia-Policy-SSO-585826603973"
export AWS_DEFAULT_REGION=${AWS_REGION:-"us-east-1"}
ln -s /.aws/ ~/.aws

# loggedIn="$(aws sts get-caller-identity)"
# if [[ "${loggedIn}" =~ "UserId" ]]; then
#   echo "Already authenticated with AWS for ${AWS_DEFAULT_PROFILE}"
# else
#   echo "Logging into AWS with profile ${AWS_DEFAULT_PROFILE}" 
#   aws sso login --profile $AWS_DEFAULT_PROFILE
# fi

# export ANSIBLE_CONFIG=/gitlab/ligre-squad/gitlab/ansible.cfg
