# Run docker container with aws sso and vault auth

``` powershell
$ aws sso login --profile Vuforia-Policy-SSO-585826603973

# login to HashiCorp Vault
## via oidc
$ vault login -method=oidc -path=atlas role=default
## via username/password
$ VAULT_ADDR="https://studio-hashicorp-vault.rd1.thingworx.io"
$ vault login -no-print -method=userpass username="${VAULT_USERNAME}" password="${VAULT_PASSWORD}"

# run container with the AWS and Vault credentials mounted for authentication
$ docker run -it --rm -v /mnt/c/Users/chanlon/.aws:/root/.aws -v /mnt/c/Users/chanlon/.vault-token:/root/.vault-token -v /mnt/c/Users/chanlon/gitlab:/gitlab docker.rd1.thingworx.io/ligre-squad/ansible-playbooks/gitlab:master bash

# verify aws credentials inside of the container
/bin/bash:/src $ aws sts get-caller-identity --profile Vuforia-Policy-SSO-585826603973

# set default aws profile. If not set, ansible probably will not use the profile
# when calling aws commands.
/bin/bash:/src $ export AWS_DEFAULT_PROFILE="Vuforia-Policy-SSO-585826603973"

# verify ansible and aws setup properly by listing hosts and pinging
/bin/bash:/src $ cd <playbook_dir>
/bin/bash:/src $ ansible all -i inventory/aws_ec2.yml --list-hosts
/bin/bash:/src $ ansible all -i inventory/aws_ec2.yml -m ping
```