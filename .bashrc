
# Start and reuse the same ssh-agent as long as it is running.
# Use in conjunction with .bash_logout
# if [ -n "$SSH_AUTH_SOCK" ] ; then
#   eval `/usr/bin/ssh-agent -k`
# fi
# in order to kill the ssh_agent on logout.
# Taken from https://unix.stackexchange.com/questions/90853/how-can-i-run-ssh-add-automatically-without-a-password-prompt/217223#217223
if [ ! -S ~/.ssh/ssh_auth_sock ]; then
    eval `ssh-agent`
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
ssh-add -l > /dev/null || ssh-add

alias home='cd ~'
function gitlab() { cd ~/gitlab; }

# quick commands to add identities to ssh-agent
function ssh-bitbucket() { ssh-add ~/.ssh/ptc_bitbucket_ed25519; }
function ssh-ccgitlab() { ssh-add ~/.ssh/ptc_gitlab_rd-services_ed25519; }

# launch into gitlab dir
gitlab
