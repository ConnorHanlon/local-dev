#

``` bash
# Renaming all files in a directory
for file in *.png; do mv "$file" "${file/_h.png/_half.png}"; done
```