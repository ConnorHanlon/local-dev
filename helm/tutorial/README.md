https://helm.sh/docs/chart_template_guide/getting_started/

The values.yaml file is also important to templates. This file contains the default values for a chart. These values may be overridden by users during helm install or helm upgrade.

The Chart.yaml file contains a description of the chart. You can access it from within a template.

The charts/ directory may contain other charts (which we call subcharts). Later in this guide we will see how those work when it comes to template rendering.

# Starter chart

``` bash
# will initialize chart directory
helm create mychart
```

If you take a look at the mychart/templates/ directory, you'll notice a few files already there.

- NOTES.txt: The "help text" for your chart. This will be displayed to your users when they run helm install.
- deployment.yaml: A basic manifest for creating a Kubernetes deployment
- service.yaml: A basic manifest for creating a service endpoint for your deployment
- _helpers.tpl: A place to put template helpers that you can re-use throughout the chart

## ConfigMap

Object for storing configuration data, `templates/configmap.yml`

After creating the initial configmap.yml, install it:

``` bash
# make kubernetes available
minikube start --driver=docker
helm install full-coral ./mychart

# If installation fails with an error like
# Error: INSTALLATION FAILED: Kubernetes cluster unreachable: Get "https://127.0.0.1:32769/version": dial tcp 127.0.0.1:32769: connectex: No connection could be made because the target machine actively refused it.
# and you are using rancher+minikube, then you must run minikube start prior to install
# $ minikube start --driver=docker

# retrieve release and the actual template that was loaded
helm get manifest full-coral

# Uninstall full-coral release
helm uninstall full-coral

# To test template rendering without installing anything
helm install --debug --dry-run full-coral ./mychart
```

# Built-in Objects

Objects can contain simple values, other objects or functions. **The built-in values always begin with a capital letter**. This is in keeping with Go's naming convention. When you create your own names, you are free to use a convention that suits your team.

Common top-level built-in objects are:

## Release
This object describes the release itself. It has several objects inside of it:
  - `Release.Name`: The release name
  - `Release.Namespace`: The namespace to be released into (if the manifest doesn’t override)
  - `Release.IsUpgrade`: This is set to true if the current operation is an upgrade or rollback.
  - `Release.IsInstall`: This is set to true if the current operation is an install.
  - `Release.Revision`: The revision number for this release. On install, this is 1, and it is incremented with each upgrade and rollback.
  - `Release.Service`: The service that is rendering the present template. On Helm, this is always Helm

## Values

`Values` passed into the template from the `values.yaml` file and from user-supplied files. By default, `Values` is empty.

## Chart

The contents of the `Chart.yaml` file. Any data in `Chart.yaml` will be accessible here. For example `{{ .Chart.Name }}-{{ .Chart.Version }}` will print out the `mychart-0.1.0`.

## Subcharts

This provides access to the scope (.Values, .Charts, .Releases etc.) of subcharts to the parent. For example `.Subcharts.mySubChart.myValue` to access the `myValue` in the `mySubChart` chart.

## Files

This provides access to all non-special files in a chart. While you cannot use it to access templates, you can use it to access other files in the chart.

- `Files.Get` is a function for getting a file by name (.Files.Get config.ini)
- `Files.GetBytes` is a function for getting the contents of a file as an array of bytes instead of as a string. This is useful for things like images.
- `Files.Glob` is a function that returns a list of files whose names match the given shell glob pattern.
- `Files.Lines` is a function that reads a file line-by-line. This is useful for iterating over each line in a file.
- `Files.AsSecrets` is a function that returns the file bodies as Base 64 encoded strings.
- `Files.AsConfig` is a function that returns file bodies as a YAML map.

## Capabilities

This provides information about what capabilities the Kubernetes cluster supports.

- `Capabilities.APIVersions` is a set of versions.
- `Capabilities.APIVersions.Has $version` indicates whether a version (e.g., batch/v1) or resource (e.g., apps/v1/Deployment) is available on the cluster.
- `Capabilities.KubeVersion` and `Capabilities.KubeVersion.Version` is the Kubernetes version.
- `Capabilities.KubeVersion.Major` is the Kubernetes major version.
- `Capabilities.KubeVersion.Minor` is the Kubernetes minor version.
- `Capabilities.HelmVersion` is the object containing the Helm Version details, it is the same output of helm version.
- `Capabilities.HelmVersion.Version` is the current Helm version in semver format.
- `Capabilities.HelmVersion.GitCommit` is the Helm git sha1.
- `Capabilities.HelmVersion.GitTreeState` is the state of the Helm git tree.
- `Capabilities.HelmVersion.GoVersion` is the version of the Go compiler used.

## Template

Contains information about the current template that is being executed

- `Template.Name`: A namespaced file path to the current template (e.g. mychart/templates/mytemplate.yaml)
- `Template.BasePath`: The namespaced path to the templates directory of the current chart (e.g. mychart/templates)

# Values

The built-in object Values provides access to values passed into the chart. Its contents come from multiple sources, which are ordered by specificity the `--set` parameter being the highest precedence:
- values.yaml
- If this is a subchart, the values.yaml file of a parent chart
- A value file passed with the `-f` flag
- Individual parameters passed with `--set` (eg `helm install --set foo=bar ./mychart`)

The values trees should generally be shallow, favoring flatness over complex value trees.

``` bash
# use default values from values.yml
helm install geared-marsup ./mychart --dry-run --debug

# override favoriteDrink with set
helm install geared-marsupi ./mychart --dry-run --debug --set favoriteDrink=tea
```

# Template Functions and Pipelines

- quote: wrap value in quotation marks
- default: used to specify a default value if not provided. It should not be defined in the values.yml, as that file should have statically defined default values. The `default` command should be used for computing values but it should not be declared inside of values.yml

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
data:
  myvalue: "Hello World"
  drink: {{ .Values.favorite.drink | default "tea" | quote }}
  food: {{ .Values.favorite.food | upper | quote }}
```

Operators are also functions (`eq`, `ne`, `lt`, `gt`, `and`, `or`, etc.) and can be grouped with parenthesis.

[Check out Helms list of template functions](https://helm.sh/docs/chart_template_guide/function_list/)

# Control Flow 
https://helm.sh/docs/chart_template_guide/control_structures/

# Named Templates

An important detail to keep in mind when naming templates: `template names are global`. If you declare two templates with the same name, whichever one is loaded `last` will be the one used. Because templates in subcharts are compiled together with top-level templates, you should be careful to name your templates with chart-specific names.

## define and template actions
You can declare a named template inside of a template file using `define` and use the named template using `template`.

By default `define` does not produce output unless it is called with `template`. Typically these templates are placed inside of a partials file `_helpers.tpl`

```yaml
{{- define "mychart.labels" }}
  labels:
    generator: helm
    date: {{ now | htmlDate }}
{{- end }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-configmap
  {{- template "mychart.labels" }}
data:
  myvalue: "Hello World"
  {{- range $key, $val := .Values.favorite }}
  {{ $key }}: {{ $val | quote }}
  {{- end }}
```

The named templates using `define` conventionally have a documentation block (`{{/* ... */}}`) describing what they do.

When using `template` to use a named template, it specifies the scope which the named template has access.  Therefore if a named template requires a scope, you can specify the scope with the secondary argument.

The following passes the top level scope that the `template` is scoped to:
```yml
{{- template "mychart.labels" . }}
```

## include vs template

The `template` action is not a function, therefore there is no way to pass the output of a template call to other functions and is simply inserted inline.

The function `include` will import the contents of the template into the present pipeline where it can be passed along to other functions in the pipeline. In fact, `include` is almost always preferrable to `template` because of this.