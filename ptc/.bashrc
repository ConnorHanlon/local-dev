. ~/env/env-vars.sh
. ~/gitlab/personal/local-dev/.bashrc

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # load nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # load nvm bash_completion

export AWS_DEFAULT_PROFILE="Vuforia-Policy-SSO-585826603973"

function aws_auth() {
  # aws sso login will direct to browser for authentication
  echo "Verify aws authentication"
  aws sts get-caller-identity &> /dev/null || exit_code=$?
  if [[ ${exit_code} -ne 0 ]]; then
    echo "Logging into aws"
    aws sso login
  fi
}

function az_auth() {
  echo "Verify azure authentication"
  az account show &> /dev/null || exit_code=$?
  if [[ ${exit_code} -ne 0 ]]; then
    echo "Logging into Azure"
    echo "Launching device login page"
    "C:\Program Files\Google\Chrome\Application\chrome.exe" "https://microsoft.com/devicelogin"
    az login --use-device-code
  fi
}

function vault_auth() {
  # vault login will direct to browser for authentication
  echo "Verify vault authentication"
  vault token lookup &> /dev/null || exit_code=$?
  if [[ ${exit_code} -ne 0 ]]; then
    echo "Logging into HashiCorp Vault"
    vault login -no-print -method=oidc -path=atlas role=default
  fi
}

function ptc_auth() {
  aws_auth
  az_auth
  vault_auth
}

ptc_auth
