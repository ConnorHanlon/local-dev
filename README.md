# local-dev

# .gitconfig

The `.gitconfig` file can be included in the global `.gitconfig` by running the command

```bash
$ git config --global include.path <gitconfig_path>
$ git config --global include.path ~/gitlab/local-dev/.gitconfig
```

# .bashrc

Auto-launches ssh-agent when bash or Git shell is opened, requesting ssh-key passphrase if set. Additionally adds command aliases.

The .bashrc file can be sourced by your local .bashrc file by adding the following line:

```bash
# source the common .bashrc file in ~/.bashrc
. <path to common bashrc>

# run the following command to add the line automatically
echo ". <path to common bashrc>" >> ~/.bashrc
echo ". ~/gitlab/local-dev/.bashrc" >> ~/.bashrc
```

## Custom .bashrc for Work

``` .bashrc
. ~/env/env-vars.sh
. ~/gitlab/personal/local-dev/.bashrc

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # load nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # load nvm bash_completion

export AWS_DEFAULT_PROFILE="Vuforia-Policy-SSO-585826603973"

# aws sso login will direct to browser for authentication
aws sso login

# vault login will direct to browser for authentication
vault login -no-print -method=oidc -path=atlas role=default

## setup vault login on startup using username/password. Recommended to use oidc instead.
# export VAULT_ADDR="https://studio-hashicorp-vault.rd1.thingworx.io"
# vault login -no-print -method=userpass username="${VAULT_USERNAME}" password="${VAULT_PASSWORD}"
```